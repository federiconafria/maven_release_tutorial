package com.amazinglyabstract;

import org.junit.Test;
import static org.junit.Assert.*;

public class MainTest{
	@Test
	public void testMessage(){
		Main main = new Main("message1");
		assertEquals("message1", main.getMessage());
	}
}