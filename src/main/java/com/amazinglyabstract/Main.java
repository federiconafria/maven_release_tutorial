package com.amazinglyabstract;

public class Main{

	private String message;

	public Main(String message) {
		this.message = message;
	}

	public String getMessage(){
		return this.message;
	}

}